# SimpleAmazon

A frontend for Amazon that allows users to browse and search amazon on any TLD of choice (like fr, de, com, co.uk, etc)

## How to install/run

```bash
go install codeberg.org/SimpleWeb/SimpleAmazon@latest
SimpleAmazon
```

## Usage

You can only specify the `host` and the `port` the program should run on using the `-h` and the `-p` flag respectively. Default values are `host:localhost` and `port:8080`
